import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	myButton:{
		alignItems:'center',
		justifyContent:'center',
		width:160,
		height:40,
		borderRadius:20
	},

	stretch: {
		width: 75,
		height: 75,
		resizeMode: 'stretch'
	},

	center: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	
	title: {
		color: '#0e5090',
		fontWeight: 'bold',
		fontSize: 26
	},
	
	text: {
		color: '#0e5090',
		fontSize: 16
	},

	prediccion: {
		fontWeight: 'bold',
		fontSize: 14
	}
	
});
