import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, Image, Text, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import DatePicker from 'react-native-date-picker'

import MyButton from './MyButton';
import styles from './Styles';


export default datosScreen = ({ route, navigation }) => {
	const { genero } = route.params;
	console.log(JSON.stringify(genero));
	const [nombre, setNombre] = React.useState('');
	const [email, setEmail] = React.useState('');
	const [fechaNacimiento, setFechaNacimiento] = React.useState(new Date());
	const [diasRestantes, setDiasRestantes] = React.useState(0);
	const [signo, setSigno] = React.useState('');
	const [scale, setScale] = useState(0);

	getSigno = (dia, mes) => {
		if((dia>=21&&mes==3)||(dia<=20&&mes==4)) return "aries";
		if((dia>=24&&mes==9)||(dia<=23&&mes==10)) return "libra";
		if((dia>=21&&mes==4)||(dia<=21&&mes==5)) return "tauro";
		if((dia>=24&&mes==10)||(dia<=22&&mes==11)) return "escorpio";
		if((dia>=22&&mes==5)||(dia<=21&&mes==6)) return "geminis";
		if((dia>=23&&mes==11)||(dia<=21&&mes==12)) return "sagitario";
		if((dia>=21&&mes==6)||(dia<=23&&mes==7)) return "cancer";
		if((dia>=22&&mes==12)||(dia<=20&&mes==1)) return "capricornio";
		if((dia>=24&&mes==7)||(dia<=23&&mes==8)) return "leo";
		if((dia>=21&&mes==1)||(dia<=19&&mes==2)) return "acuario";
		if((dia>=24&&mes==8)||(dia<=23&&mes==9)) return "virgo";
		if((dia>=20&&mes==2)||(dia<=20&&mes==3)) return "piscis";
	}

	useEffect(()=> {
		var hoy = new Date();
		var proximoCumpleanios = new Date();
		var nacimiento = new Date(fechaNacimiento);
		setSigno(getSigno(nacimiento.getDate(), nacimiento.getMonth()+1));
		
			
		proximoCumpleanios.setDate(nacimiento.getDate());
		proximoCumpleanios.setMonth(nacimiento.getMonth());
		
		//Si este año aún no cumple años
		if(hoy.getTime() > proximoCumpleanios.getTime()) {
			proximoCumpleanios.setFullYear(hoy.getFullYear() + 1);
		}
		var diasProximoCumpleanios = (proximoCumpleanios.getTime() - hoy.getTime()) / (1000*60*60*24)
		console.log(diasProximoCumpleanios);
		setDiasRestantes(diasProximoCumpleanios);
		
		if(nombre !== '' && email !== '')
			setScale(1);
	})

	return (

	<View style={{flex: 1, flexDirection: 'column'}}>

		<View style={[ styles.center, {flex: 1}]}>
			<Text style={styles.title}>INGRESA TUS DATOS</Text>
		</View>

		<View style={[ styles.center, {flex: 3, padding: 20}]}>

			<View style={[ styles.center, {flex: 1, flexDirection: 'row'}]}>
				<View style={{flex: 1}}>
					<Text style={styles.text}>Nombre:</Text>
				</View>
				<View style={{flex: 3}}>
					<TextInput keyboardType="default" style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
					onChangeText={text => setNombre(text)} value={nombre}/>
				</View>
			</View>

			<View style={[ styles.center, {flex: 1, flexDirection: 'row'}]}>
				<View style={{flex: 1}}>
					<Text style={styles.text}>Email:</Text>
				</View>
				<View style={{flex: 3}}>
					<TextInput keyboardType="default" style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
					onChangeText={text => setEmail(text)} value={email}/>
				</View>
			</View>
			
			<View style={[ styles.center, {flex: 2, flexDirection: 'row'}]}>
				<View style={{flex: 1}}>
					<Text style={styles.text}>Fecha de Nacimiento:</Text>
				</View>
				<View style={{flex: 3}}>					
					<DatePicker mode="date" androidVariant="nativeAndroid" date={fechaNacimiento} onDateChange={setFechaNacimiento} />
				</View>
			</View>

		</View>
		
		<View style={[ styles.center, {flex: 1}]}>			
			<View style={[ styles.center, {flex: 1, flexDirection: 'column', justifyContent: 'space-around'}]}>
				<View style={{flex: 1}}>
					<MyButton text="VOLVER" bgcolor="#0e5090" onPress={() => navigation.goBack()} />
				</View>
				<View style={{flex: 1}}>
					<MyButton scale={scale} text="CONTINUAR" onPress={() => navigation.navigate('Horoscopo', {signo: signo, nombre:nombre, diasRestantes})} />
				</View>
			</View>
		</View>

	</View>
	
	);
}

