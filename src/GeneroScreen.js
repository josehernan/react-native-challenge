import React, { useState } from 'react';
import { TouchableOpacity, View, Image, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import MyButton from './MyButton';
import styles from './Styles';


export default GeneroScreen = ({ navigation }) => {
	const [genero, setGenero] = useState(null);
	const [scale, setScale] = useState(0);

	return (
	<View style={{flex: 1, flexDirection: 'column'}}>

		<View style={[ styles.center, {flex: 1}]}>
			<Text style={styles.title}>INGRESA TU GÉNERO</Text>
		</View>


		<View style={[ styles.center, {flex: 1}]}>
	
			<View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>

				<View style={{flex: 1}}> 
					<TouchableOpacity onPress={() => { setGenero("M"); setScale(1) } } style={styles.myButton} >
						<Image style={styles.stretch} source={require('./images/m.png')} />
					</TouchableOpacity>
				</View>

				<View style={{flex: 1}}>
					<TouchableOpacity onPress={() => { setGenero("MF"); setScale(1) } } style={styles.myButton} >
						<Image style={styles.stretch} source={require('./images/mf.png')} />
					</TouchableOpacity>
				</View>

				<View style={{flex: 1}}>
					<TouchableOpacity onPress={() => { setGenero("F"); setScale(1) } } style={styles.myButton} >
						<Image style={styles.stretch} source={require('./images/f.png')} />
					</TouchableOpacity>
				</View>

			</View>

		</View>

		<View style={[ styles.center, {flex: 1}]}>
			<MyButton scale={scale} text="INGRESAR" onPress={() => navigation.navigate('Datos', {genero:genero} )} />
		</View>
      
	</View>

	);
}

