import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './Styles';

export default MyButton = (props) => {
	return (
	<TouchableOpacity onPress={props.onPress} style={[styles.myButton, {backgroundColor: props.bgcolor, transform: [{ scale: props.scale }] } ]}>
		<Text style={{ color: "#FFFFFF" }}>{props.text}</Text>
	</TouchableOpacity>
	);
}

MyButton.defaultProps = {
	bgcolor: '#fe6d1a',
	scale: 1
};
