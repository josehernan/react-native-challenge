import React from 'react';
import { View, Image, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import MyButton from './MyButton';
import styles from './Styles';


export default HomeScreen = ({ navigation }) => {
	return (
	<View style={{flex: 1, flexDirection: 'column'}}>

		<View style={[ styles.center, {flex: 3}]}>
			<Text style={styles.title} >Home Screen</Text>
		</View>

		<View style={[ styles.center, {flex: 1}]}>
			<MyButton text="INGRESAR" onPress={() => navigation.navigate('Genero')} />
		</View>

	</View>
	);
}
