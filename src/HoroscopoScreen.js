import React, { useState, useEffect } from 'react';
import { ActivityIndicator, TouchableOpacity, View, Image, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import MyButton from './MyButton';
import styles from './Styles';


export default HoroscopoScreen = ({ route, navigation }) => {
	console.log("Horoscopo");

	const { nombre } = route.params;
	const { signo } = route.params;
	const { diasRestantes } = route.params;
	const [loading, setLoading] = React.useState(true);
	const [error, setError] = React.useState(false);
	const [horoscopo, setHoroscopo] = React.useState('');
 
	const imagenesSigno = {
		aries: require('./images/boton_aries.png'),
		tauro: require('./images/boton_tauro.png'),
		geminis: require('./images/boton_geminis.png'),
		cancer: require('./images/boton_cancer.png'),
		leo: require('./images/boton_leo.png'),
		virgo: require('./images/boton_virgo.png'),
		libra: require('./images/boton_libra.png'),
		escorpion: require('./images/boton_escorpion.png'),
		sagitario: require('./images/boton_sagitario.png'),
		capricornio: require('./images/boton_capricornio.png'),
		acuario: require('./images/boton_acuario.png'),
		piscis: require('./images/boton_piscis.png'),
	}

	var imagenSigno;
	
	switch (signo) {
		case "aries": imagenSigno = imagenesSigno.aries; break;
		case "tauro": imagenSigno = imagenesSigno.tauro; break;
		case "geminis": imagenSigno = imagenesSigno.geminis; break;
		case "cancer": imagenSigno = imagenesSigno.cancer; break;
		case "leo": imagenSigno = imagenesSigno.leo; break;
		case "virgo": imagenSigno = imagenesSigno.virgo; break;
		case "libra": imagenSigno = imagenesSigno.libra; break;
		case "escorpion": imagenSigno = imagenesSigno.escorpion; break;
		case "sagitario": imagenSigno = imagenesSigno.sagitario; break;
		case "carpicornio": imagenSigno = imagenesSigno.capricornio; break;
		case "acuario": imagenSigno = imagenesSigno.acuario; break;
		case "piscis": imagenSigno = imagenesSigno.piscis; break;
	}

async function getHoroscopo () {
	try {
	const response = await fetch('https://api.adderou.cl/tyaas')
	const h = await response.json()

	var mensajeCumpleanios;
	if(diasRestantes === 0) { mensajeCumpleanios = "¡¡¡Feliz cumpleaños!!!"; }
	if(diasRestantes === 1) {	mensajeCumpleanios = "¡¡¡Mañana es tu cumpleaños!!!"; }
	if(diasRestantes > 1) {	mensajeCumpleanios = "Faltan " + diasRestantes + " días para tu cumpleaños"; }
	
	for (var s in h.horoscopo) {
		if(s === signo ) {
			console.log(h.horoscopo[s])
			var prediccion = h.horoscopo[s];
			var hoy = "Tu horóscopo para hoy dice" + 
			"\n\nSalud: " + prediccion.salud + 
			"\n\nDinero: " + prediccion.dinero + 
			"\n\nAmor: " + prediccion.amor +
			"\n\nColor de la suerte: " + prediccion.color +
			"\nNúmero de la suerte: " + prediccion.numero +
			"\n\n" + mensajeCumpleanios;
			
			console.log(hoy);
			setHoroscopo(hoy);
		}
	}

	//setHoroscopo('contenido')
	
	setLoading(false);
	} catch (e) {
	setLoading(false);
	setError(true);
	}
}

useEffect(()=> {
	if(horoscopo === "")
		getHoroscopo()
})

	if (loading) {
		return (
		<View style={styles.center}>
			<ActivityIndicator size="large" color="#fe6d1a" />
		</View>
		)
	}

	if (error) {
		return (
		<View style={styles.center}>
			<Text>Error</Text>
		</View>
		)
	}

	return (   
	<View style={{flex: 1, flexDirection: 'column', padding: 15}}>

		<View style={[ styles.center, {flex: 1}]}>
			<Image source={imagenSigno} />
		</View>

		<View style={[ styles.center, {flex: 3}]}>
			<Text style={styles.title}>Hola {nombre}</Text>
			<Text style={styles.prediccion}>{horoscopo}</Text>
		</View>

		<View style={[ styles.center, {flex: 1}]}>
			<MyButton text="CONTINUAR" onPress={() => navigation.navigate('Home')} />
		</View>
	</View>
	);
}
