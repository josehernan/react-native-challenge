import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Button, View, Image, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MyButton from './src/MyButton';
import styles from './src/Styles';

import HomeScreen from './src/HomeScreen';
import GeneroScreen from './src/GeneroScreen';
import DatosScreen from './src/DatosScreen';
import HoroscopoScreen from './src/HoroscopoScreen';

function DetailsScreen({ navigation }) {
  return (  
    <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 2, backgroundColor: 'powderblue'}} />
        <View style={{flex: 2, backgroundColor: 'skyblue'}} />
        <View style={{flex: 2, backgroundColor: 'steelblue'}} />
      </View>
  );
}

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">

        <Stack.Screen name="Home" component={HomeScreen}  options={{ headerShown: false }} />
        
        <Stack.Screen name="Genero" component={GeneroScreen} options={{
          title: 'Tu Género',
          headerStyle: {
            backgroundColor: '#0e5090',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitleAlign : 'center'
        }}
        />
        
        <Stack.Screen name="Datos" component={datosScreen} options={{
          title: 'Tus datos',
          headerStyle: {
            backgroundColor: '#0e5090',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitleAlign : 'center'
        }}
        />

        <Stack.Screen name="Horoscopo" component={HoroscopoScreen} options={{
          title: 'Tu Horóscopo',
          headerStyle: {
            backgroundColor: '#0e5090',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitleAlign : 'center'
        }}
        />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}


